# Engine NODE(S)

> Short blurb about what your product does.

[![Build Status](https://img.shields.io/badge/build-passing-brightgreen.svg)](https://atozgo.co.id)
[![Coverage](https://img.shields.io/badge/coverage-100%25-brightgreen.svg)](https://atozgo.co.id)
[![Ruby Version](https://img.shields.io/badge/ruby%20version-2.4.1-blue.svg)](https://atozgo.co.id)
[![Rails Version](https://img.shields.io/badge/rails%20version-5.2.0-blue.svg)](https://atozgo.co.id)
[![Version](https://img.shields.io/badge/version-1.0.0-lightgrey.svg)](https://atozgo.co.id)

One to two paragraph statement about your product and what it does.

![atozpay](public/atozgo.png)


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.


## Prerequisites

What things you need to install the software and how to install them

```sh
Give examples
```


## Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```ruby
Give the example
```

And repeat

```ruby
until finished
```

End with an example of getting some data out of the system or using it for a little demo


## Deployment

Add additional notes about how to deploy this on a live system


## Release History

* 0.2.1
    * CHANGE: Update docs (module code remains unchanged)
* 0.2.0
    * CHANGE: Remove `setDefaultXYZ()`
    * ADD: Add `init()`
* 0.1.1
    * FIX: Crash when calling `baz()` (Thanks @GenerousContributorName!)
* 0.1.0
    * The first proper release
    * CHANGE: Rename `foo()` to `bar()`
* 0.0.1
    * Work in progress


## Contributors

* **Dede Badru** - *Initial commit* - [dedebadru](https://github.com/dedebadru)


## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc


## TODO
* [ ] unchecked
* [x] checked


## Contributing
1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D
