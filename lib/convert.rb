module Convert
  # conver phonenumber with prefix
  def self.with_prefix(phonenumber = "")
    phonenumber = self.clean_char(phonenumber)
    phonenumber = phonenumber.try(:sub, "0", "62") if phonenumber.try(:first).try(:eql?, "0")

    return phonenumber
  end

  # conver phonenumber without prefix
  def self.without_prefix(phonenumber = "")
    phonenumber = self.clean_char(phonenumber)
    phonenumber = phonenumber.try(:sub, "62", "0") if phonenumber.try(:first, 2).try(:eql?, "62")

    return phonenumber
  end

private
  # conver clean char
  def self.clean_char(text = "")
    text = text.try(:gsub, /[ +-]/, "")

    return text
  end
end
