# inspiration on reference devise/encryptor.rb
module Atozauth
  class Encryptor
    # File 'lib/devise/encryptor.rb', line 14
    def self.compare(klass, hashed_password, password)
      return false if hashed_password.blank?
      bcrypt   = ::BCrypt::Password.new(hashed_password)
      password = "#{password}#{klass.pepper}" if klass.pepper.present?
      password = ::BCrypt::Engine.hash_secret(password, bcrypt.salt)

      return self.parent.secure_compare(password, hashed_password)
    end

    # File 'lib/devise/encryptor.rb', line 7
    def self.digest(klass, password)
      password = "#{password}#{klass.pepper}" if klass.pepper.present?

      return ::BCrypt::Password.create(password, cost: klass.stretches).to_s
    end

    # HMAC Encryptor
    def self.hmac(data, options = {})
      options[:algorithm] = "sha1" if options[:algorithm].blank?
      options[:key]       = "" if options[:key].blank?
      digest              = OpenSSL::Digest.new(options[:algorithm].to_s)
      instance            = OpenSSL::HMAC.new(options[:key].to_s, digest)
      instance.update(data.to_s)

      return instance
    end
  end
end
