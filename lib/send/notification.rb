module Send
  class Notification
    def self.firebase(options = {})
      options[:device] = "android" if options.try(:class).try(:eql?, Hash) && options[:device].blank?

      return self.send([__method__.try(:to_s), "_", options[:device].try(:to_s).try(:parameterize, separator: "_").try(:downcase)].try(:join).try(:to_sym), options)
    end

    def self.onesignal(options = {})
      options[:app] = "user" if options.try(:class).try(:eql?, Hash) && options[:app].blank?

      return self.send([__method__.try(:to_s), "_", options[:app].try(:to_s).try(:parameterize, separator: "_").try(:downcase)].try(:join).try(:to_sym), options)
    end

  private
    def self.firebase_android(options = {})
      options[:fcm_tokens]        = [] if options.try(:class).try(:eql?, Hash) && options[:fcm_tokens].blank?
      options[:data]              = {} if options.try(:class).try(:eql?, Hash) && options[:data].blank?
      options[:priority]          = "normal" if options.try(:class).try(:eql?, Hash) && options[:priority].blank?
      options[:content_available] = true if options.try(:class).try(:eql?, Hash) && options[:content_available].blank?
      options[:notification]      = {} if options.try(:class).try(:eql?, Hash) && options[:notification].blank?
      options[:app]               = Rpush::Gcm::App.find_by_name("android_app") if options.try(:class).try(:eql?, Hash) && options[:app].blank?

      notification = Rpush::Gcm::Notification.create(
        app: options[:app],
        registration_ids: options[:fcm_tokens],
        data: options[:data],
        priority: options[:priority],
        content_available: options[:content_available],
        notification: options[:notification]
      )

      return notification
    end

    def self.onesignal_user(options = {})
      options[:app_id]        = ENV["ONESIGNAL.APPID"] if options.try(:class).try(:eql?, Hash) && options[:app_id].blank?
      options[:authorization] = ENV["ONESIGNAL.AUTHORIZATION"] if options.try(:class).try(:eql?, Hash) && options[:authorization].blank?

      return self.onesignal_request(options)
    end

    def self.onesignal_agen(options = {})
      options[:app_id]        = ENV["ONESIGNAL.AGEN.APPID"] if options.try(:class).try(:eql?, Hash) && options[:app_id].blank?
      options[:authorization] = ENV["ONESIGNAL.AGEN.AUTHORIZATION"] if options.try(:class).try(:eql?, Hash) && options[:authorization].blank?

      return self.onesignal_request(options)
    end

    def self.onesignal_request(options = {})
      options[:app]           = "user" if options.try(:class).try(:eql?, Hash) && options[:app].blank?
      options[:app_id]        = "" if options.try(:class).try(:eql?, Hash) && options[:app_id].blank?
      options[:authorization] = "" if options.try(:class).try(:eql?, Hash) && options[:authorization].blank?
      options[:player_ids]    = [] if options.try(:class).try(:eql?, Hash) && options[:player_ids].blank?
      options[:segments]      = [] if options.try(:class).try(:eql?, Hash) && options[:segments].blank?
      options[:tags]          = [] if options.try(:class).try(:eql?, Hash) && options[:tags].blank?
      options[:data]          = {} if options.try(:class).try(:eql?, Hash) && options[:data].blank?
      options[:contents]      = {en: ""} if options.try(:class).try(:eql?, Hash) && options[:contents].blank?
      options[:headings]      = {en: ""} if options.try(:class).try(:eql?, Hash) && options[:headings].blank?
      options[:params]        = {} if options.try(:class).try(:eql?, Hash) && options[:params].blank?

      options[:params].merge!({
        app_id: options[:app_id],
        contents: options[:contents],
        headings: options[:headings],
        data: options[:data],
        android_background_data: true
      })

      options[:params].merge!({
        tags: options[:tags]
      }) if options[:tags].present?

      options[:params].merge!({
        include_player_ids: options[:player_ids]
      }) if options[:player_ids].present?

      options[:params].merge!({
        included_segments: options[:segments]
      }) if options[:segments].present?

      return self.parent.send(:request, ENV["ONESIGNAL.HOST"], method: :post, ssl: true, headers: {"Content-Type" => "application/json", "Authorization" => options[:authorization]}, body_params: options[:params])
    end
  end
end
