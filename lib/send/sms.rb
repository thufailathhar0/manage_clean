require "uri"

module Send
  class Sms
    # atoz server master send sms
    def self.atozsms(phonenumber = "", options = {})
      phonenumber        = Convert.without_prefix(phonenumber)
      options[:text]     = "" if options.try(:class).try(:eql?, Hash) && options[:text].blank?
      options[:provider] = (["0855", "0856", "0857", "0858", "0814", "0815", "0816"].try(:include?, phonenumber).try(:first, 4) ? "indosat" : "simpati") if options.try(:class).try(:eql?, Hash) && options[:provider].blank?
      options[:response] = {"code" => 402, "messages" => "Failed."}

      if options.try(:class).try(:eql?, Hash) && options[:url].blank?
        options[:url]       = URI.parse(ENV.fetch("SMS.ENDPOINT"){""})
        options[:url].query = URI.encode_www_form(
          "phonenumber" => phonenumber,
          "text" => options[:text],
          "provider" => options[:provider]
        )
        options[:url]       = options[:url].to_s
      end

      begin
        response = self.parent.request(options[:url], type_response: :pure)

        return response.body.try(:eql?, "halo string") ? {"code" => 200, "messages" => "Success."} : options[:response]

      rescue
        return options[:response]

      end
    end

    # infobip master send sms
    def self.infobip(phonenumber = "", options = {})
      phonenumber             = Convert.with_prefix(phonenumber)
      options[:url]           = [ENV.fetch("INFO_BIP.URI"){""}, "/sms/1/text/single"].join if options.try(:class).try(:eql?, Hash) && options[:url].blank?
      options[:from]          = ENV.fetch("INFO_BIP.SMS_SENDER"){""} if options.try(:class).try(:eql?, Hash) && options[:from].blank?
      options[:text]          = "" if options.try(:class).try(:eql?, Hash) && options[:text].blank?
      options[:auth_user]     = ENV.fetch("INFO_BIP.USER"){""} if options.try(:class).try(:eql?, Hash) && options[:auth_user].blank?
      options[:auth_password] = ENV.fetch("INFO_BIP.PASSWORD"){""} if options.try(:class).try(:eql?, Hash) && options[:auth_password].blank?

      response = self.parent.request(options[:url], body_params: {from: options[:from], to: phonenumber, text: options[:text]}, basic_auth: {user: options[:auth_user], password: options[:auth_password]}, method: :post, ssl: true)

      return response
    end
  end
end
