require "net/http"
require "uri"

module Send
  # send sms by environment
  def self.sms(phonenumber = "", options = {})
    ::Rails.env.try(:production?) ? ::Send::Sms.infobip(phonenumber, options) : ::Send::Sms.atozsms(phonenumber, options)
  end

  # request http with handler
  def self.request(url = "", options = {})
    options[:headers]          = {"Content-Type" => "application/json"} if options.try(:class).try(:eql?, Hash) && (!options[:headers].try(:class).try(:eql?, Hash) || options[:headers].blank?)
    options[:type_response]    = :body if options.try(:class).try(:eql?, Hash) && options[:type_response].blank?
    options[:default_response] = {"meta"=> {"code"=> 402, "messages" => "Terjadi kesalahan, silahkan coba kembali atau hubungi customer service."}, "data" => {}} if options.try(:class).try(:eql?, Hash) && (!options[:default_response].try(:class).try(:eql?, Hash) || options[:default_response].blank?)
    options[:body_params]      = {} if options.try(:class).try(:eql?, Hash) && options[:body_params].blank?
    options[:ssl]              = false if options.try(:class).try(:eql?, Hash) && options[:ssl].blank?
    options[:method]           = :get if options.try(:class).try(:eql?, Hash) && options[:method].blank?
    uri                        = URI.parse(url)
    response                   = self.send_request(uri, options)

    begin
      return options[:type_response].try(:eql?, :body) ? JSON.parse(response.body) : response

    rescue
      return options[:default_response]

    end
  end

private
  # http block
  def self.http(uri, options = {})
    options[:default_response] = {"meta"=> {"code"=> 402, "messages" => "Terjadi kesalahan, silahkan coba kembali atau hubungi customer service."}, "data" => {}} if options.try(:class).try(:eql?, Hash) && (!options[:default_response].try(:class).try(:eql?, Hash) || options[:default_response].blank?)

    begin
      http         = Net::HTTP.new(uri.try(:host), uri.try(:port))
      http.use_ssl = true if options[:ssl].try(:eql?, true)

      return http

    rescue
      return options[:default_response]

    end
  end

  # request method block
  def self.http_request(uri, options = {})
    options[:default_response] = {"meta"=> {"code"=> 402, "messages" => "Terjadi kesalahan, silahkan coba kembali atau hubungi customer service."}, "data" => {}} if options.try(:class).try(:eql?, Hash) && (!options[:default_response].try(:class).try(:eql?, Hash) || options[:default_response].blank?)

    begin
      request      = [Net::HTTP, "::", options[:method].try(:to_s).try(:parameterize, separator: "_").try(:camelize)].try(:join).try(:constantize).try(:new, uri.try(:request_uri), options[:headers])
      request.body = options[:body_params].to_json if options.try(:class).try(:eql?, Hash) && options[:body_params].present?
      request.basic_auth(options[:basic_auth][:user], options[:basic_auth][:password]) if options.try(:class).try(:eql?, Hash) && options[:basic_auth].present? && options[:basic_auth].try(:class).try(:eql?, Hash)

      return request

    rescue
      return options[:default_response]

    end
  end

  # request block
  def self.send_request(uri, options = {})
    options[:default_response] = {"meta"=> {"code"=> 402, "messages" => "Terjadi kesalahan, silahkan coba kembali atau hubungi customer service."}, "data" => {}} if options.try(:class).try(:eql?, Hash) && (!options[:default_response].try(:class).try(:eql?, Hash) || options[:default_response].blank?)

    begin
      http    = self.http(uri, options)
      request = self.http_request(uri, options)

      return http.request(request)

    rescue
      return options[:default_response]

    end
  end
end
