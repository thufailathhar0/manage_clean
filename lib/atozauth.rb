# inspiration on reference devise.rb
module Atozauth
  # File 'lib/devise.rb', line 492
  def self.friendly_token(length = 20)
    # To calculate real characters, we must perform this operation.
    # See SecureRandom.urlsafe_base64
    rlength = (length * 3) / 4
    SecureRandom.urlsafe_base64(rlength).tr('lIO0', 'sxyz')
  end

  # File 'lib/devise.rb', line 500
  def self.secure_compare(a, b)
    return false if a.blank? || b.blank? || a.bytesize != b.bytesize
    l = a.unpack "C#{a.bytesize}"

    res = 0
    b.each_byte { |byte| res |= byte ^ l.shift }
    res == 0
  end

  # Encode JWT Token
  def self.encode(payload)
    payload.merge!({
      iss: Base64.encode64(ENV.fetch("JWT.ISSUER"){""}),
      aud: Base64.encode64(ENV.fetch("JWT.AUDIENCE"){[]}),
      sub: Base64.encode64(ENV.fetch("JWT.SUBJECT"){""})
    })

    return JWT.encode(payload, ENV.fetch("JWT.SECRET"){nil}, ENV.fetch("JWT.ALGORITHM"){"none"}, {
      appname: Base64.encode64(ENV.fetch("AUTH.APPNAME"){""}),
      appid: Base64.encode64(ENV.fetch("AUTH.APPID"){""}),
      appsecret: Base64.encode64(ENV.fetch("AUTH.APPSECRET"){""})
    })
  end

  # Decode JWT Token
  def self.decode(token)
    return JWT.decode(token, ENV.fetch("JWT.SECRET"){nil}, true, {
      iss: Base64.encode64(ENV.fetch("JWT.ISSUER"){""}),
      aud: Base64.encode64(ENV.fetch("JWT.AUDIENCE"){[]}),
      sub: Base64.encode64(ENV.fetch("JWT.SUBJECT"){""}),
      algorithm: ENV.fetch("JWT.ALGORITHM"){"none"},
      verify_iss: true,
      verify_aud: true,
      verify_sub: true
    })
  end

  # Generate OTP code
  def self.generate_otp(digit = 6)
    return (self.random(digit).to_s * 2 ).try(:last, digit)
  end

  # Generate Client ID
  def self.generate_id(digit = 40)
    return self.random(digit)
  end

  # Generate Secret
  def self.generate_secret(digit = 40)
    return self.secret(digit)
  end

  # Generate expire time, default 1 hour
  def self.expire(time = 1*1000*60*60)
    return ((Time.now.to_i * 1000) + time).floor
  end
private
  # File 'config/devise.rb'
  def self.pepper
    return nil
  end

  # File 'config/devise.rb'
  def self.stretches
    return 10
  end

  # Master Random Number
  def self.random(digit = 10)
    return rand(10 ** digit)
  end

  # Master Secret Character
  def self.secret(digit = 10)
    return SecureRandom.hex(digit)
  end
end
