Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "application#index"

  scope :api do
    root "application#index"

    namespace :v1 do
      resources :dashboard, only: :index
    end

    namespace :v6 do
      scope :cleaning do
        resources :manages, controller: "cleanings/manages"
      end
    end
  end

  match "*path", to: "application#notfound", via: :all
  match "", to: "application#notfound", via: :all
end
