write_db_file       = File.join(Rails.root, "config", "database", "write.yml")
db_config_file      = File.exists?(write_db_file) ? write_db_file : File.join(Rails.root, "config", "database.yml")
WRITE_DB_CONNECTION = YAML.load_file(db_config_file)[Rails.env.to_s]
