Rails.application.credentials[Rails.env.to_sym].each do |key, value|
  ENV[key.to_s] = value.to_s
end if Rails.try(:application).try(:credentials).try(:config).try(:present?) && Rails.try(:application).try(:credentials)[Rails.try(:env).try(:to_sym)]
