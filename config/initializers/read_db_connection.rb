read_db_file       = File.join(Rails.root, "config", "database", "read.yml")
db_config_file     = File.exists?(read_db_file) ? read_db_file : File.join(Rails.root, "config", "database.yml")
READ_DB_CONNECTION = YAML.load_file(db_config_file)[Rails.env.to_s]
