class ApplicationMailer < ActionMailer::Base
  default from: "no-reply@atozgo.co.id"
  layout 'mailer'
end
