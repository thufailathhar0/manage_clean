require "sidekiq"

class UpdatePointCouriersWorker
  include Sidekiq::Worker

  def perform(order_id, couriers={})
    if couriers.present?
      logger = Logger.new("#{Rails.root}/log/not_response_courier.log")
      count_courier = 0
      courier_id = []
      # status_ids_sibuk = Status.where("code > 100 AND code NOT IN (?)", [103, 104, 108, 109]).pluck(:id)

      order = Write::Order.where("id = ?", order_id).first
      status = Write::Status.find_by(code: "101")
      logger.info("===== START ORDER ID #{order.id} =====")
      logger.info("kurir: #{couriers}")
      if order.status_id != status.id && order.courier_id.blank?
        couriers.each do |courier|
          courier = Write::Courier.find(courier['id'])
          next if courier.courier_busy?
            courier_id << courier['id']
            count_courier = count_courier+1
            Write::Courier.find_by(id: courier['id']).update_attributes(minus_points: courier['minus_points'].to_i+-1)
        end
      end

      logger.info("courier id: #{courier_id}")
      logger.info("total couriers get minus points: #{count_courier}")
      logger.info("===== END ORDER ID #{order.id} =====")
    end
  end
end
