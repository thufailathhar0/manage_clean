class ApplicationController < ActionController::API
  before_action :validate_apikey, :validate_version, :validate_user

  def index
    return render json: construct_response("success", {
      messages: ["atozgo bit engine."]
    })
  end

  def notfound
    return render status: 404, json: construct_response("error", {
      code: 404,
      messages: ["not found."]
    })
  end

private
  def construct_response(type = "error", options = {})
    options            = {}          if !options.try(:class).try(:to_s).try(:include?, "Hash")
    options[:code]     = 200         if options[:code].blank?
    options[:messages] = ["success"] if options[:messages].blank?
    options[:data]     = {}          if options[:data].blank? && !options[:data].try(:class).try(:to_s).try(:include?, "Array")
    options[:meta]     = {}          if options[:meta].blank?
    options[:return]   = {}

    return options[:return].merge!({
      meta: options[:meta].merge!({
        code:     options[:code],
        messages: options[:messages]
      }),
      data: options[:data]
    })
  end

  def validate_apikey
    apikey = Read::ApiKey.where(client_key: request.env.fetch("HTTP_CLIENTKEY", "")).first

    if apikey.blank?
      return render status: 402, json: construct_response("error", {
        code: 402,
        messages: ["invalid cient key."]
      })
    end
  end

  def validate_user
    @current_user = Read::User.where(auth_token: auth_token).first

    if auth_token.blank? || @current_user.blank?
      return render status: 401, json: construct_response("error", {
        code: 401,
        messages: ["not authorized."]
      })
    end
  end

  def validate_version
    if request.env.fetch("HTTP_APPS_VERSION", "").present? && request.env.fetch("HTTP_APPS_ORIGIN", "").present?
      app_version = Read::AppVersion.where("app_version = ? AND apps_origin = ? AND status = ?", request.env.fetch("HTTP_APPS_VERSION", ""), request.env.fetch("HTTP_APPS_ORIGIN", ""), true).first

      return render json: construct_response("warning", {
        code: 409,
        messages: ["opps, currently you are using old version, please update your apps version."]
      }) if app_version.blank?

      current_user.update_attribute(:app_version, request.env.fetch("HTTP_APPS_VERSION", "")) if current_user.present? && !current_user.app_version.eql?(request.env.fetch("HTTP_APPS_VERSION", ""))

    else
      return render json: construct_response("warning", {
        code: 409,
        messages: ["opps, unknown apps version. please update your apps."]
      })

    end
  end

  def authorization_token
    request.env.fetch("HTTP_AUTHORIZATION", "").scan(/Bearer (.*)$/).flatten.last
  end

  def decode_authorization_token
    Atozauth.decode(authorization_token).first rescue {}
  end

  def auth_token
    if params[:auth_token].present?
      @auth_token = params[:auth_token]

    else
      @auth_token = decode_authorization_token["auth_token"]

    end
  end

  def current_user
    @current_user
  end
end

