class V1::DashboardController < V1Controller
  def index
    return render json:  construct_response("success", {
      messages: ["dashboard v1."]
    })
  end
end
