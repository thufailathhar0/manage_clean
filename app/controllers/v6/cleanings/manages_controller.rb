class V6::Cleanings::ManagesController < V6::CleaningsController
	skip_before_action :validate_apikey, :validate_version, :validate_user, only: [:index, :create, :show, :update]


	def index
		@manage = ManageCleaning.order(created_at: :desc)
		if @manage.present?
			render json: construct_response("success",{
				messages: ["success"],
				data: @manage.as_json
			})
		else
			render json: construct_response("warning",{
				messages: ["warning"],
				data: []
			})
		end
	end

	def create
		@manage = ManageCleaning.new(params_manage_cleaning)
		if @manage.save
			render json: construct_response("success",{
				messages: ["success"],
				data: @manage.as_json
			})
		else
			render json: construct_response("warning",{
				messages: ["warning"],
				data: {}
			})
		end
	end

	def show
		@manage = ManageCleaning.find(params[:id])
		if @manage.present?
			render json: construct_response("success",{
				messages: ["success"],
				data: @manage.as_json
			})
		else
			render json: construct_response("warning",{
				messages: ["warning"],
				data: {}
			})
		end
	end

	def update
		@manage = ManageCleaning.find(params[:id])
		if @manage.update(params_manage_cleaning)
			render json: construct_response("success",{
				messages: ["success"],
				data: @manage.as_json
			})
		else
			render json: construct_response("warning",{
				messages: ["warning"],
				data: {}
			})
		end
	end

private

	def params_manage_cleaning
		params.require(:manage_cleaning).permit(:name, :description, :price)
	end
end
